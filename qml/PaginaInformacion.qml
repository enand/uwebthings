/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.12
import Ubuntu.Components 1.3

Page {
    id: paginaInformacion
    visible: false
    property string texto : i18n.tr('<p style=\"text-indent: 75px; margin-bottom: 50px;\" >Gracias por usar Webthings para ubports, ésta aplicación está desarrollada para satisfacer una necesidad personal, sin embargo es posible que tú también le encuentres utilidad.</p><p style=\"text-indent: 75px; margin-bottom: 50px;\" >Es una aplicación que se encuentra en sus primeros estados de desarrollo y podrás encontrar errores y cuelgues inesperados.</p><p style=\"text-indent: 75px; margin-bottom: 50px;\">Para poder utilizar la app debes tener instalado, configurado y en funcionamiento la <a href = \"https://webthings.io\">pasarela Webthings en tu red local.</a></p><p style=\"text-indent: 75px; margin-bottom: 50px;\" >Quedan muchas funciones todavía que desarrollar pero puede ser un buen comienzo para gestionar tu propio internet de las cosas personal.</p><p style=\"text-indent: 75px; margin-bottom: 50px;\" >No ha sido testada con todos los posibles dispositivos que admite Webthings, eso quiere decir que es posible que alguno de ellos no funcione correctamente en la aplicación.</p><p style=\"text-indent: 75px; margin-bottom: 50px;\" >Si ves utilidad a la aplicación y puedes/quieres colaborar para continuar con su desarrollo, puedes enviar una ayuda a la siguiente dirección de Bitcoin Cash (bch) (Por favor no la confundas con Bitcoin (btc)).</p>')

    header: PageHeader {
        id: header2
        title: i18n.tr('Información')
        leadingActionBar{

            actions: [
                Action {
                    iconName: "go-home"
                    text: i18n.tr('Webthings')
                    onTriggered: pageStack.push(paginaInicial)
                },
                Action{
                    iconName: "settings"
                    text: i18n.tr('Configuración')
                    onTriggered: pageStack.push(paginaConfiguracion)
                }

            ]
        }
    }
    Flickable {
      id:flickableInfo
      clip: true
      anchors.left:parent.left
      anchors.right:parent.right
      anchors.top:parent.top
      anchors.topMargin : header2.height + units.gu(2)
      anchors.bottom:parent.bottom
      bottomMargin: units.gu(2)
      contentHeight: textCuerpo.height + icono.height + qrBch.height + recDireccionBch.height + units.gu(10)
      Image {
        id : 'icono'
        anchors.top : parent.top
        anchors.topMargin : units.gu(0.5)
        anchors.horizontalCenter : parent.horizontalCenter
        width : units.gu(9)
        height : units.gu(9)
        source: '../assets/logo96.png'
      }
      Text{
        id:textCuerpo
        anchors.top : icono.bottom
        anchors.left : parent.left
        anchors.leftMargin : units.gu(2)
        anchors.right : parent.right
        anchors.rightMargin : units.gu(2)
        anchors.topMargin : units.gu(2)
        font.pixelSize: FontUtils.sizeToPixels("large")
        wrapMode: Text.WordWrap
        width : parent.width
        horizontalAlignment : Text.AlignLeft
        font.family: "Ubuntu"
        textFormat: Text.RichText
        onLinkActivated: Qt.openUrlExternally(link)
        text:texto
      }
      Image {
        id : 'qrBch'
        anchors.top : textCuerpo.bottom
        anchors.topMargin : units.gu(1)
        anchors.horizontalCenter : parent.horizontalCenter
        fillMode : Image.PreserveAspectFit
        width : parent.width * 0.8
        source: '../assets/Webthings.svg'
      }
      Rectangle{
        id : recDireccionBch
        color:"transparent"
        width:parent.width
        height:textDireccion.height
        anchors.top : qrBch.bottom
        anchors.topMargin : units.gu(2)
        Rectangle{
          id:recBotonCopiar
          width:parent.width * 0.2
          height:textDireccion.height
          anchors.left:parent.left
          color:"transparent"
          ButtonSimbol{
            id:copyAddress
            sizeButton:4
            iconButton:"edit-copy"
            MouseArea {
                anchors.fill: parent
                ToolTip.text:"Dirección copiada al portapapeles"
                ToolTip.timeout: 3000
                onClicked: {
                  ToolTip.visible = true
                  Clipboard.push(textDireccion.text);
                }
            }
          }
        }
        Text{
          id:textDireccion
          anchors.top : parent.top
          anchors.left : recBotonCopiar.right
          anchors.leftMargin : units.gu(2)
          anchors.right : parent.right
          anchors.rightMargin : units.gu(2)
          font.pixelSize: FontUtils.sizeToPixels("medium")
          wrapMode: Text.WrapAnywhere
          width : parent.width * 0.8
          horizontalAlignment : Text.AlignLeft
          font.family: "Ubuntu"
          textFormat: Text.RichText
          text:'bitcoincash:qzcrsu92drpaq7k5jh5wq7p75c9yp7xn0sd0wk9agr'
        }
      }
    }


    /*Component.onCompleted: {
      python.call('PaginaInicial.leerTexto', [root.appFolder.toString(), 'info.txt'], function(resultado){texto = resultado});
    }*/
}
