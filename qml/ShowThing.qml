/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtWebSockets 1.0

Page {
    id: showThing
    property var listaPropiedades : []
    property var ordenPropiedades : {}
    header: PageHeader {
        id: header
        title: i18n.tr(root.titleCosaActiva)
        ActionBar {
          numberOfSlots: 1
          actions:[
            Action {
                iconName: "back"
                text: i18n.tr('Webthings')
                onTriggered: {
                  socketThing.active = false
                  pageStack.push(paginaInicial, {'activarSocket':true})
                }
            }
          ]
        }
        trailingActionBar {
          actions: [
              Action {
                  // Controlo el icono que nos indica que tipo de conexion usamos y si está activa
                  id : 'botonRedThing'
                  checkable : true
                  iconName: {
                    if (root.errorConexion){
                      if (root.wifi){'preferences-network-wifi-no-connection-symbolic'}
                      else {'transfer-error'}
                    }
                    else {
                      if (root.wifi){"preferences-network-wifi-active-symbolic"}
                      else {'transfer-progress'}
                    }
                  }
                  text: "first"
                  onToggled:{
                    if (root.wifi) {root.wifi = false; root.jwtValido = root.jwtRemote}
                    else {root.wifi = true; root.jwtValido = root.jwt}
                  }
              }
         ]
         numberOfSlots: 1
      }
    }
    ActivityIndicator {
      id: activityProperties
      anchors.centerIn:parent
      height: units.gu(5)
    }
      GridView {
        id: 'presentacionPropiedades'
        height : units.gu(70)
        width:parent.width
        anchors.top : header.bottom
        anchors.topMargin : units.gu(2)
        anchors.left : parent.left
        anchors.leftMargin : units.gu(1)
        cellWidth: parent.width * 0.5
        cellHeight: units.gu(20)
        model: listaPropiedades
        delegate: contactDelegate
        focus: true

      Component {
        id: contactDelegate
        Rectangle {
          x : presentacionPropiedades.cellWidth * 0.05
          y : presentacionPropiedades.cellHeight * 0.05
          width : presentacionPropiedades.cellWidth * 0.9
          height : presentacionPropiedades.cellHeight * 0.9
          radius : units.gu(1)
          color : '#a5c1cc'
          //anchors.centerIn : parent

          Loader {
            id : 'cargadorComponente'
            // Cargo el elemento dependiendo del tipo de dato que sea
            anchors.top : parent.top
            anchors.left : parent.left
            width : parent.width
            height : parent.height * 0.7

            Component.onCompleted: {
                var qml = ''
                var onOff = ['AlarmProperty', 'BooleanProperty','LeakProperty','MotionProperty','OnOffProperty','OpenProperty','PushedProperty','SmokeProperty']
                var sliderProperty = ['BrightnessProperty','HumidityProperty','LevelProperty', 'ColorTemperatureProperty']
                //Comprobamos primero si el valor es de tipo enumerado
                if (listaPropiedades[index][1].hasOwnProperty('enum')){
                  setSource( 'EnumProperty.qml')
                }
                else{
                  if ((onOff.includes(listaPropiedades[index][1]['@type']) || listaPropiedades[index][1]['type'] == 'boolean' ) && !(listaPropiedades[index][1]['readOnly'])) {
                    // Si es uno de los tipos interruptor y escritura/lectura permitido
                    setSource( 'OnOffProperty.qml')
                  }
                  else {
                    if (sliderProperty.includes(listaPropiedades[index][1]['@type']) && !(listaPropiedades[index][1]['readOnly'])){
                      // Si es uno de los tipos slide y escritura/lectura permitido
                      setSource('SliderProperty.qml')
                    }
                    else {
                      //Miramos si es la propiedad de color
                      if(listaPropiedades[index][1]['@type'] == 'ColorProperty'){
                        setSource( 'ColorProperty.qml')
                      }
                      else{
                        // si es boolean la presentamos como icono
                        if (listaPropiedades[index][1]['type'] == 'boolean'){
                          setSource( 'BooleanProperty.qml')
                        }
                        else {
                          // Cualquier otro, lo sacamos como texto
                          setSource( 'TextProperty.qml')
                        }
                      }

                    }
                  }
                }

            }
          }
          Text {
            id: 'nombre'
            anchors.top : cargadorComponente.bottom
            anchors.topMargin : units.gu(1)
            anchors.left : parent.left
            //anchors.leftMargin : units.gu(2)
            width : parent.width
            height : parent.height * 0.3
            horizontalAlignment : Text.AlignHCenter
            clip : true
            wrapMode : Text.WordWrap
            font.pixelSize: FontUtils.sizeToPixels("medium")
            text: listaPropiedades[index][1]['title']
          }
        }
      }
      WebSocket {
          id: socketThing
          url: {
            if (root.wifi){'ws://' + root.ip + ':8080/things/' + root.idCosaActiva + '?jwt=' + root.jwtValido}
            else {'wss://' + root.dominioLimpio + '/things/' + root.idCosaActiva + '?jwt=' + root.jwtValido}
          }
          property var propiedad:''
          property var nuevoValor:''

          onTextMessageReceived: {
            var resultSocketThing = JSON.parse(message)
            var indice = 0
            if(resultSocketThing.messageType == 'propertyStatus'){
              propiedad = Object.keys(resultSocketThing.data)[0]
              nuevoValor = resultSocketThing.data[propiedad]
              indice = ordenPropiedades[propiedad]
              listaPropiedades[indice][1]['value'] = nuevoValor
              // Actualizo el model para que el listview muestre los cambios
              presentacionPropiedades.model = listaPropiedades
            }
          }
          onStatusChanged: if (socketThing.status == WebSocket.Error) {
                              root.errorConexion = true
                               console.log("Error: " + socket.errorString)
                           } else if (socketThing.status == WebSocket.Open) {
                                root.errorConexion = false
                               console.log('Socket opened ++++++++++++++++++++++++++++++++++++++')
                           } else if (socketThing.status == WebSocket.Closed) {
                              root.errorConexion = true
                               console.log("Socket closed***************************************")
                           }
          active: false
      }
    }
    Component.onCompleted: {
      activityProperties.running = true
      python.call('PaginaInicial.readProperties', [root.jwtValido, root.wifi, root.ip, root.dominio, root.idSelectThing], function(datos){
        if (datos[0] == 'ERROR'){
          msjError.msj = datos[1]
          msjError.visible = true
        }
        else {listaPropiedades = datos[0]; ordenPropiedades = datos[1]}
        socketThing.active = true
        activityProperties.running = false
      });
    }
    ColorPicker{
      id : 'emergenteColor'
      modal : true
    }
    EmergenteError{
      id : msjError
      modal : true
      onClosed : {
        socketThing.active = false
        pageStack.push(paginaInicial)
      }

    }
}
