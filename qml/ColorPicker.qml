/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQuick 2.12
import QtQuick.Controls 2.2



Popup {
    id: emergenteColorPicker
    width: parent.width * 0.8
    height: parent.height * 0.6
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    background: Rectangle{
      width:parent.width
      radius:units.gu(1)
      color:"white"
    }
    property var listaColores : ["#ff0000","#ff4000","#ff8000","#ffbf00","#ffff00","#bfff00","#80ff00","#40ff00","#00ff00","#00ff40","#00ff80","#00ffbf","#00ffff","#00bfff","#0080ff","#0040ff","#0000ff","#4000ff","#8000ff","#bf00ff","#ff00ff","#ff00bf","#ff0080","#ff0040","#ff0000"]
    property var msjEnvioSocket : ''
    property var indicePropiedad : 0

    Flickable {
      id:flickableColorPicker
      clip: true
      anchors.left:parent.left
      anchors.right:parent.right
      anchors.top:parent.top
      anchors.topMargin : units.gu(2)
      anchors.bottom:cerrar.top
      bottomMargin: units.gu(2)
      contentHeight: colores.height

    GridView {
      id : 'colores'
      width: parent.width
      height: units.gu(30)
      cellWidth: colores.width * 0.20
      cellHeight: units.gu(6)

      Component {
          id: contactsDelegate
          Rectangle {
              id: wrapper
              width: colores.cellWidth
              height: colores.cellHeight
              color: listaColores[index]
              MouseArea {
                  anchors.fill: parent
                  onClicked: {
                    msjEnvioSocket = '{\"messageType\": \"setProperty\", \"data\" :{\"' + listaPropiedades[indicePropiedad][1]['name'] + '\":\"' + listaColores[index] + '\"}}'
                    socketThing.sendTextMessage(msjEnvioSocket)
                    close()
                  }
              }
          }
      }

      model: listaColores
      delegate: contactsDelegate
      focus: true
    }
  }

  Button {
    id: "cerrar"
    text: 'Cancelar'
    anchors.bottom : parent.bottom
    anchors.bottomMargin : units.gu(2)
    height : units.gu(6)
    width : units.gu(15)
    anchors.horizontalCenter : parent.horizontalCenter
    onClicked: close()
  }


}
