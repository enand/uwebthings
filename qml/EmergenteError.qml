/*
 * Copyright (C) 2021  ehf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * MuhfazaCoin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.7
import QtQuick.Controls 2.2

Popup {
    id: dialog
    width: parent.width * 0.8
    height: parent.height * 0.2
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    background: Rectangle{
      width:parent.width
      radius:units.gu(1)
      color:"white"
      border.color: 'black'
    }
    property string msj:''

    Flickable {
      id:flickableDialog
      clip: true
      anchors.left:parent.left
      anchors.right:parent.right
      anchors.top:parent.top
      anchors.bottom:pieEmergente.top
      bottomMargin: units.gu(2)
      contentHeight: columnDialog.height + units.gu(5)
      Column{
        id:columnDialog
        width:parent.width
        anchors.top:parent.top
        spacing:units.gu(2)
        Text{
          id:tituloEmergente
          font.pixelSize: FontUtils.sizeToPixels("large")
          wrapMode: Text.WordWrap
          anchors.left:parent.left
          anchors.leftMargin:units.gu(2)
          anchors.right:parent.right
          anchors.rightMargin:units.gu(2)
          //color:g.colorTextoEmergente
          font.family: "Ubuntu"
          textFormat: Text.RichText
          text:i18n.tr(msj)
        }
      }
    }
    Row{
      id:pieEmergente
      anchors.bottom:parent.bottom
      width:parent.width
      height:units.gu(4)
      Rectangle{
        id:aceptarRectangle
        radius : units.gu(1)
        width:parent.width
        height:parent.height
        color:"transparent"
        Mybutton{
          id:aceptarButton
          textButton:"Aceptar"
          onClicked:close()
        }
      }
    }



}
