/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 import QtQuick 2.12
 import Ubuntu.Components 1.3
 import QtQuick.Controls 2.2

 Rectangle{
   width : parent.width
   height : parent.height
   anchors.centerIn : parent
   color : 'transparent'
   property var msjEnvioSocket : '{\"messageType\": \"setProperty\", \"data\" :{\"' + listaPropiedades[index][1]['name'] + '\":' + control.checked + '}}'

   Switch {
     id : control
     checked : listaPropiedades[index][1]['value']
     property bool interruptor : false
     property string url : ''
     property string namePropiedad : ''
     anchors.centerIn : parent
     width : parent.width * + 0.4
     height : units.gu(3)
     enabled : listaPropiedades[index][1]['readOnly'] ? false : true
     indicator: Rectangle {
      implicitWidth: parent.width
      implicitHeight: units.gu(3)
      x: control.leftPadding
      y: parent.height / 2 - height / 2
      radius: 13
      color: control.checked ? "#17a81a" : "#ffffff"
      border.color: control.checked ? "#17a81a" : "#cccccc"

      Rectangle {
          x: control.checked ? parent.width - width : 0
          width: parent.width * 0.5
          height: units.gu(3)
          radius: 13
          color: control.down ? "#cccccc" : "#ffffff"
          border.color: control.checked ? (control.down ? "#17a81a" : "#21be2b") : "#999999"
      }
    }
    onToggled : {
      socketThing.sendTextMessage(msjEnvioSocket)
    }
  }
}
