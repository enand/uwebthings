/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
Column{
    anchors.top: parent.top
    anchors.topMargin: 130
    anchors.left: parent.left
    anchors.right: parent.right
    Item{
        /*anchors.top: cabeceraConfiguracion.bottom
        anchors.topMargin: 30*/
        anchors.left: parent.left
        anchors.right: parent.right
        id: "ipLocal"


        Text {
            id: "etiquetaIp"
            anchors.left:parent.left
            anchors.leftMargin: 15

            font.pointSize: 13
            text:i18n.tr("IP Local:")
        }

        TextField {
            id: "campoIp"
            inputMask: "000.000.000.000;_"
            anchors.right: parent.right
            anchors.rightMargin: 20
            anchors.bottom: etiquetaIp.bottom
            placeholderText: qsTr(i18n.tr("Dirección IP local:"))
            color: "red"
            /*background: Rectangle {
                id: txt_back
                anchors.fill: campoIp.width
                color: control.enabled ? "transparent" : "#353637"
                border.color: control.enabled ? "#21be2b" : "transparent"
                //color: "white"
            }*/

        }

    }
    Item{
        anchors.top: ipLocal.bottom
        anchors.topMargin: 50
        anchors.left: parent.left
        anchors.right: parent.right
        id: "dominio"


        Text {
            id: "etiquetaDominio"
            //anchors.bottom: linea2.bottom
            anchors.left:parent.left
            anchors.leftMargin: 15

            font.pointSize: 13
            text:i18n.tr("Dominio: ")
        }

        TextField{
            id: "campoDominio"
            anchors.right:parent.right
            anchors.rightMargin: 20
            anchors.bottom: etiquetaDominio.bottom
            placeholderText: qsTr(i18n.tr("Dominio del gateway"))
        }
    }
    Item{
        anchors.top: dominio.bottom
        anchors.topMargin: 50
        anchors.left: parent.left
        anchors.right: parent.right
        id: "usuario"


        Text {
            id: "etiquetaUsuario"
            anchors.left:parent.left
            anchors.leftMargin: 15
            font.pointSize: 13
            text:i18n.tr("Usuario: ")
        }

        TextField{
            id: "campoUsuario"
            inputMethodHints: Qt.ImhEmailCharactersOnly
            anchors.right:parent.right
            anchors.rightMargin: 20
            anchors.bottom: etiquetaUsuario.bottom
            placeholderText: qsTr(i18n.tr("Nombre de usuario"))
        }
    }
    Item{
        anchors.top: usuario.bottom
        anchors.topMargin: 50
        anchors.left: parent.left
        anchors.right: parent.right
        id: "contrasena"


        Text {
            id: "etiquetaContrasena"
            anchors.left:parent.left
            anchors.leftMargin: 15
            font.pointSize: 13
            text:i18n.tr("Contraseña: ")
        }

        TextField{
            id: "campoContrasena"
            anchors.right:parent.right
            anchors.rightMargin: 20
            anchors.bottom: etiquetaContrasena.bottom
            echoMode: TextInput.Password
            placeholderText: qsTr(i18n.tr("Contraseña"))
        }
    }
    Item{
        anchors.top: contrasena.bottom
        anchors.topMargin: 50
        anchors.left: parent.left
        anchors.right: parent.right
        id: "autorizacion"


        Text {
            id: "etiquetaAutorizacion"
            anchors.left:parent.left
            anchors.leftMargin: 15
            font.pointSize: 13
            text:i18n.tr("Autorizacion: ")
        }

        TextField{
            id: "campoAutorizacion"
            anchors.right:parent.right
            anchors.rightMargin: 20
            anchors.bottom: etiquetaAutorizacion.bottom
            placeholderText: qsTr(i18n.tr("Autorizacion"))
        }
    }
}
