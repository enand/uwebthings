/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2

Rectangle{
    property string message: ""
    property string nombre: ""
    property string valorEscrito: ""
    property bool mail : false
    property bool password : false

    Text {
        id: "etiqueta"
        width : parent.width * 0.25
        anchors.left:parent.left
        anchors.leftMargin: units.gu(2)
        wrapMode: Text.WordWrap
        anchors.verticalCenter : parent.verticalCenter
        font.pixelSize: FontUtils.sizeToPixels('medium')
        text:i18n.tr(message + ":")
    }
    Rectangle{
      id: 'recIcono'
      width :parent.width * 0.1
      height :parent.height
      anchors.left : etiqueta.right
      Icon {
         id : 'punto'
         anchors.left : parent.left
         anchors.leftMargin : units.gu(1)
         width: units.gu(3)
         height: units.gu(3)
         anchors.centerIn : parent
         visible : true
         color : 'green'
         name:  'tick'
      }
    }
    Rectangle{
      width : parent.width * 0.65
      height : parent.height
      anchors.left : recIcono.right
      TextField {
          id: "campo"
          anchors.left : parent.left
          anchors.leftMargin : units.gu(1)
          anchors.verticalCenter : parent.verticalCenter
          width : parent.width * 0.8
          font.pixelSize: FontUtils.sizeToPixels("medium")
          text: valorEscrito
          inputMethodHints: mail ? Qt.ImhEmailCharactersOnly : Qt.ImhNone
          echoMode: password ? TextInput.Password : TextInput.Normal
          placeholderText: qsTr(i18n.tr(message))
          background: Rectangle {
              implicitWidth: parent.width * 0.8
              implicitHeight : units.gu(5)
              radius: 10
              color: "transparent"
              border.color: campo.focus ? "lightblue" : "grey"
              border.width: campo.focus ? 3 : 1
          }
          onEditingFinished:{
              python.call('PaginaInicial.validarCampo', [ nombre, campo.text ], function(returnValue) {
                  if (returnValue == true) {
                    punto.name = 'tick'
                    punto.color = 'green'
                    valorEscrito = campo.text
                  }
                  else {
                    punto.name = 'edit-clear'
                    punto.color = 'red'
                    valorEscrito = ""
                  };
              });
          }
      }
    }

}
