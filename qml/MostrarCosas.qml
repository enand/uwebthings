/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

GridView {

    id: 'miGridView'
    height : units.gu(70)
    width:parent.width
    anchors.left : parent.left
    anchors.leftMargin : units.gu(1)
    cellWidth: parent.width * 0.5
    cellHeight: units.gu(20)
    model: showThings.datos
    delegate: listaCosas
    //highlight: Rectangle { width:parent.width; height: units.gu(13); color: "lightsteelblue"; radius: units.gu(2) }
    focus: true


    Component {
        id: listaCosas
        Rectangle{
          id:"rect"
          x : miGridView.cellWidth * 0.05
          y : miGridView.cellHeight * 0.05
          width : miGridView.cellWidth * 0.9
          height : miGridView.cellHeight * 0.9
          radius : units.gu(1)
          color : '#a5c1cc'
          Icon {
             id : 'iconoAvisoNoConectado'
             width: units.gu(3)
             height: units.gu(3)
             anchors.top : parent.top
             anchors.topMargin : units.gu(0.5)
             anchors.right : parent.right
             anchors.rightMargin : units.gu(0.5)
             visible : showThings.datos[index]['conectado']? false : true
             name:  'transfer-error'
          }
          Rectangle{
            id : 'recIcono'
            anchors.top : parent.top
            anchors.topMargin : units.gu(1)
            anchors.horizontalCenter : parent.horizontalCenter
            width : units.gu(10)
            height : units.gu(10)
            radius : units.gu(5)
            //color : showThings.datos[index]['encendido'] ? 'green':'transparent'
            border.width : 10
            border.color : showThings.datos[index]['encendido'] ? 'green':'transparent'
            Image {
              id : 'icono'
              anchors.top : parent.top
              anchors.topMargin : units.gu(0.5)
              anchors.horizontalCenter : parent.horizontalCenter
              opacity: showThings.datos[index]['conectado'] ? 1 : 0.3
              width : units.gu(9)
              height : units.gu(9)
              source: '../assets/devices/' + showThings.datos[index]['icono']
            }
          }
          Text {
            id : 'titulo'
            font.pixelSize: FontUtils.sizeToPixels("small")
            anchors.top: recIcono.bottom
            anchors.topMargin : units.gu(2)
            anchors.left : parent.left
            anchors.leftMargin : units.gu(1)
            width:parent.width * 0.9
            horizontalAlignment : Text.AlignHCenter
            clip : true
            wrapMode : Text.WordWrap
            text: showThings.datos[index]['title']

          }


          MouseArea {

            anchors.fill: parent
            onClicked: {
                miGridView.currentIndex = index
                root.idSelectThing = showThings.datos[index]['id']
                root.iconoActivo = showThings.datos[index]['icono']
                root.titleCosaActiva = showThings.datos[index]['title']
                root.idCosaActiva = showThings.datos[index]['idlimpio']
                //Detengo el socket
                activarSocket = false
                socket.active = false
                pageStack.push(paginaShowthing)


            }
          }
        }
    }

}
//}
