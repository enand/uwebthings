/*
 * Copyright (C) 2021  ehf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * MuhfazaCoin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3

Popup {
    id: dialog
    width: parent.width * 0.8
    height: parent.height * 0.6
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    background: Rectangle{
      width:parent.width
      radius:units.gu(1)
      color:"white"
      border.color: 'black'
    }
    property string msj:''
    property bool runActivities : true

    Flickable {
      id:flickableDialog
      clip: true
      anchors.left:parent.left
      anchors.right:parent.right
      anchors.top:parent.top
      anchors.bottom:pieEmergente.top
      bottomMargin: units.gu(2)
      contentHeight: textTitulo.height + gridDialog.height + textExplicacion.height + units.gu(5)
      Text{
        id:textTitulo
        anchors.top : parent.top
        anchors.topMargin : units.gu(2)
        font.pixelSize: FontUtils.sizeToPixels("large")
        wrapMode: Text.WordWrap
        width : parent.width
        horizontalAlignment : Text.AlignHCenter
        font.family: "Ubuntu"
        textFormat: Text.RichText
        text:i18n.tr('Comprobando conexión')
      }
      Grid{
        id:gridDialog
        width:parent.width
        anchors.top:textTitulo.bottom
        columns : 2
        spacing:units.gu(2)
        Text{
          id:textLocal
          font.pixelSize: FontUtils.sizeToPixels("medium")
          wrapMode: Text.WordWrap
          width : parent.width * 0.5
          height : units.gu(6)
          horizontalAlignment : Text.AlignRight
          verticalAlignment : Text.AlignVCenter
          //color:g.colorTextoEmergente
          font.family: "Ubuntu"
          textFormat: Text.RichText
          text:i18n.tr('Conexión local: ')
        }
        Rectangle{
          id : 'recIconoLocal'
          width : parent.width * 0.5
          height : units.gu(6)
          ActivityIndicator {
            id: activityLocal
            anchors.left : parent.left
            anchors.leftMargin : units.gu(1)
            anchors.verticalCenter : parent.verticalCenter
            height: units.gu(5)
            running : runActivities
          }
          Icon {
             id : 'iconoLocal'
             width: units.gu(3)
             height: units.gu(3)
             anchors.left : parent.left
             anchors.leftMargin : units.gu(1)
             anchors.verticalCenter : parent.verticalCenter
             visible : {
               if (estadoConexion == ''){false}
               else {true}
             }
             color : {
               if (estadoConexion == 2 || estadoConexion == 0){'green'}
               else{'red'}
             }
             name:{
               // Compruebo si tenemos ya algun resultado, sino no mostramos ningún icono

               if (estadoConexion == 2 || estadoConexion == 0){'tick'}
               else{'edit-clear'}


             }
          }
        }
        Text{
          id:textRemoto
          font.pixelSize: FontUtils.sizeToPixels("medium")
          wrapMode: Text.WordWrap
          width : parent.width * 0.5
          height : units.gu(6)
          horizontalAlignment : Text.AlignRight
          verticalAlignment : Text.AlignVCenter
          //color:g.colorTextoEmergente
          font.family: "Ubuntu"
          textFormat: Text.RichText
          text:i18n.tr('Pasarela remota: ')
        }
        Rectangle{
          id : 'recIconoRemoto'
          width : parent.width * 0.5
          height : units.gu(6)
          ActivityIndicator {
            id: activityRemote
            anchors.left : parent.left
            anchors.leftMargin : units.gu(1)
            anchors.verticalCenter : parent.verticalCenter
            height: units.gu(5)
            running : runActivities
          }
          Icon {
             id : 'iconoRemoto'
             width: units.gu(3)
             height: units.gu(3)
             anchors.left : parent.left
             anchors.leftMargin : units.gu(1)
             anchors.verticalCenter : parent.verticalCenter
             visible :{
               if (estadoConexion == ''){false}
               else {true}
             }
             color : {
               if (estadoConexion == 2 || estadoConexion == 1){'green'}
               else{'red'}
             }
             name:{
               // Compruebo si tenemos ya algun resultado, sino no mostramos ningún icono
               if (estadoConexion == 2 || estadoConexion == 1){'tick'}
               else{'edit-clear'}
             }
          }
        }
      }
      Text{
        id:textExplicacion
        anchors.top : gridDialog.bottom
        font.pixelSize: FontUtils.sizeToPixels("medium")
        wrapMode: Text.WordWrap
        width : parent.width
        height : units.gu(12)
        horizontalAlignment : Text.AlignHCenter
        verticalAlignment : Text.AlignVCenter
        //color:g.colorTextoEmergente
        font.family: "Ubuntu"
        textFormat: Text.RichText
        text:i18n.tr(msj)
      }
    }
    Row{
      id:pieEmergente
      anchors.bottom:parent.bottom
      width:parent.width
      height:units.gu(4)
      Rectangle{
        id: recVolverIntentar
        radius : units.gu(1)
        width:parent.width * 0.5
        height:parent.height
        color:"transparent"
        Mybutton{
          id:intentarButton
          textButton:"Volver a intentar"
          onClicked:{
            msj = ''
            estadoConexion = ''
            close()
            runActivities = true
          }
        }
      }
      Rectangle{
        id:recGuardar
        radius : units.gu(1)
        width:parent.width * 0.5
        height:parent.height
        color:"transparent"
        Mybutton{
          id:guardarButton
          visible : {
            if(estadoConexion == 'ERROR'){false}
            else {true}
          }
          textButton:"Terminar"
          onClicked:{
            msj = ''
            estadoConexion = ''
            pageStack.push(paginaInicial)
            close()
          }
        }
      }
    }



}
