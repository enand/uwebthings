/*
 * Copyright (C) 2021  ehf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * MuhfazaCoin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import Ubuntu.Components 1.3
import QtQuick 2.7
import QtQuick.Controls 2.2

Rectangle{
  property var sizeButton:8
  property var iconButton:""
  property var colorIcono:"white"
  property var colorFondoIcono:'lightblue'
  property var colorBordeRectagulo:'lightgrey'
  anchors.horizontalCenter: parent.horizontalCenter
  anchors.verticalCenter:parent.verticalCenter
  width:units.gu(sizeButton)
  height:units.gu(sizeButton)
  radius:units.gu(1)
  color: colorFondoIcono
  border.color:colorBordeRectagulo
  Icon {
    anchors.centerIn:parent
    width: units.gu(sizeButton-2)
    height: units.gu(sizeButton-2)
    name: iconButton
    color:colorIcono
  }


}
