/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 import QtQuick 2.12
 import Ubuntu.Components 1.3
 import QtQuick.Controls 2.2
 
Rectangle {
  id : 'casillaPropiedad'
  width : parent.width
  height : parent.height
  anchors.centerIn : parent
  color : 'transparent'
  property var msjEnvioSocket : '{\"messageType\": \"setProperty\", \"data\" :{\"' + listaPropiedades[index][1]['name'] + '\":' + control.value + '}}'

  Slider {
    id: control
    anchors.centerIn : parent
    property var valor : 0
    enabled : listaPropiedades[index][1]['readOnly'] ? false : true
    from : listaPropiedades[index][1]['minimum']
    to : listaPropiedades[index][1]['maximum']
    value : listaPropiedades[index][1]['value']

    background: Rectangle {
        x: control.leftPadding
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: casillaPropiedad.width * 0.8
        implicitHeight: units.gu(1)
        width: control.availableWidth
        height: implicitHeight
        radius: 2
        color: "#bdbebf"

        Rectangle {
            width: control.visualPosition * parent.width
            height: parent.height
            color: "#21be2b"
            radius: 2
        }
    }
    handle: Rectangle {
        x: control.leftPadding + control.visualPosition * (control.availableWidth - width)
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 26
        implicitHeight: units.gu(3)
        radius: 13
        color: control.pressed ? "#f0f0f0" : "#f6f6f6"
        border.color: "#bdbebf"
    }

    onMoved:{
      //console.log(msjEnvioSocket)
      socketThing.sendTextMessage(msjEnvioSocket)
    }
  }
}
