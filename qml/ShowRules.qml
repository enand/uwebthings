/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Ubuntu.Components 1.3 as Ut
import Ubuntu.Components.Popups 1.3
import QtQuick.Controls 2.12


Page {
    id: showRules
    property var listaReglas : []
    property bool interruptor : false
    property string idRegla : ''
    anchors.fill : parent


    header : Ut.PageHeader {
        id: header
        title: i18n.tr('Reglas, (Only view)')
        Ut.ActionBar {
          numberOfSlots: 1
          actions:[
            Ut.Action {
                iconName: "back"
                text: i18n.tr('Webthings')
                onTriggered: {
                  pageStack.push(paginaInicial, {'activarSocket':true})
                }
            }
          ]
        }
       trailingActionBar {
          actions: [
              Ut.Action {
                  // Controlo el icono que nos indica que tipo de conexion usamos y si está activa
                  id : 'botonRedRules'
                  checkable : true
                  iconName: {
                      if (root.wifi){"preferences-network-wifi-active-symbolic"}
                      else {'transfer-progress'}
                  }
                  text: "first"
                  onToggled:{
                    if (root.wifi) {root.wifi = false; root.jwtValido = root.jwtRemote}
                    else {root.wifi = true; root.jwtValido = root.jwt}
                  }
              }
         ]
         numberOfSlots: 1
      }
    }

    Ut.ActivityIndicator {
      id: activityRules
      anchors.centerIn:parent
      height: units.gu(5)
    }
    GridView {
      id: 'presentacionReglas'
      height : units.gu(70)
      width:parent.width
      y : showRules.header.height + 7
      anchors.left : parent.left
      anchors.leftMargin : units.gu(1)
      cellWidth: parent.width * 0.5
      cellHeight: units.gu(20)
      model: listaReglas
      delegate: contactDelegate
      focus: true

    Component {
      id: contactDelegate
      Rectangle {
        x : presentacionReglas.cellWidth * 0.05
        y : presentacionReglas.cellHeight * 0.05
        width : presentacionReglas.cellWidth * 0.9
        height : presentacionReglas.cellHeight * 0.9
        radius : units.gu(1)
        color : '#a5c1cc'
        Rectangle{
          id : 'recComponente'
          anchors.top : parent.top
          anchors.topMargin : units.gu(2)
          width : parent.width
          height : parent.height * 0.6
          color : 'transparent'
          Rectangle{
            width : parent.width
            height : parent.height
            anchors.centerIn : parent
            color : 'transparent'
            Switch {
              id : control
              enabled : false
              checked : listaReglas[index]['enabled']
              property bool interruptor : false
              property string url : ''
              property string namePropiedad : ''
              anchors.centerIn : parent
              width : parent.width * + 0.4
              height : units.gu(3)
              indicator: Rectangle {
               implicitWidth: parent.width
               implicitHeight: units.gu(3)
               x: control.leftPadding
               y: parent.height / 2 - height / 2
               radius: 13
               color: control.checked ? "#17a81a" : "#ffffff"
               border.color: control.checked ? "#17a81a" : "#cccccc"

               Rectangle {
                   x: control.checked ? parent.width - width : 0
                   width: parent.width * 0.5
                   height: units.gu(3)
                   radius: 13
                   color: control.down ? "#cccccc" : "#ffffff"
                   border.color: control.checked ? (control.down ? "#17a81a" : "#21be2b") : "#999999"
               }
             }
           }
         }
        }
        Text {
          id: 'nombre'
          anchors.top : recComponente.bottom
          anchors.left : parent.left
          width : parent.width
          height : parent.height * 0.4
          horizontalAlignment : Text.AlignHCenter
          clip : true
          wrapMode : Text.WordWrap
          font.pixelSize: FontUtils.sizeToPixels("medium")
          text: listaReglas[index]['name']
        }
      }
    }
  }
  Component.onCompleted: {
    activityRules.running = true
    python.call('PaginaInicial.readRules', [root.jwtValido, root.wifi, root.ip, root.dominio], function(datos){
      if (datos[0] == 'ERROR'){
        msjError.msj = datos[1]
        msjError.visible = true
      }
      else {listaReglas = datos}
      activityRules.running = false
    });
  }

  EmergenteError{
    id : msjError
    modal : true
    onClosed : {
      pageStack.push(paginaInicial)
    }

  }
}
