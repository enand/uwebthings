/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Ubuntu.Components 1.3
import QtQuick 2.12
import QtQuick.Controls 2.2




Popup {
    id: ventanaEmergente
    width: parent.width * 0.8
    height: parent.height * 0.8
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    background: Rectangle{
      width:parent.width
      radius:units.gu(1)
      color:"Lightblue"
    }
    property string textoTitulo:i18n.tr("Ayuda")
    property string texto1:i18n.tr("En la parte superior derecha hay 2 botones, el primero empezando por la derecha es el botón que permite intercambiar la conexión entre la red local y la pasarela en internet si tienes ambos interfaces configurados.")
    property string texto2:i18n.tr("El segundo botón sirve para actualizar el listado de Things que se muestran en la pantalla, si has añadido o quitado elementos en el gateway y no se muestran ahora, pulsa sobre éste botón para que se actualice")
    property string textoBoton:i18n.tr("Entendido")

    Flickable {
      id:flickableIntro
      clip: true
      anchors.left:parent.left
      anchors.right:parent.right
      anchors.top:parent.top
      anchors.topMargin : units.gu(2)
      anchors.bottom:cerrar.top
      bottomMargin: units.gu(2)
      contentHeight: icono.height + titulo.height + cuerpo.height + recorte1.height + cuerpo2.height + recorte2.height + units.gu(10)
      Image {
        id : 'icono'
        anchors.top : parent.top
        anchors.topMargin : units.gu(0.5)
        anchors.horizontalCenter : parent.horizontalCenter
        width : units.gu(9)
        height : units.gu(9)
        source: '../assets/logo.png'
      }
      Text{
        id: "titulo"
        anchors.top : icono.bottom
        anchors.topMargin : units.gu(2)
        anchors.left : parent.left
        anchors.leftMargin : units.gu(2)
        anchors.right : parent.right
        anchors.rightMargin : units.gu(2)
        font.pixelSize: FontUtils.sizeToPixels("large")
        wrapMode: Text.WordWrap
        width : parent.width
        horizontalAlignment : Text.AlignHCenter
        font.family: "Ubuntu"
        textFormat: Text.RichText
        text: textoTitulo
      }

      Text {
        id : 'cuerpo'
        anchors.top : titulo.bottom
        anchors.topMargin : units.gu(2)
        anchors.left : parent.left
        anchors.leftMargin : units.gu(2)
        anchors.right : parent.right
        anchors.rightMargin : units.gu(2)
        font.pixelSize: FontUtils.sizeToPixels("medium")
        wrapMode: Text.WordWrap
        width : parent.width
        horizontalAlignment : Text.AlignLeft
        font.family: "Ubuntu"
        textFormat: Text.RichText
        onLinkActivated: Qt.openUrlExternally(link)
        text: texto1
      }
      Image {
        id : 'recorte1'
        anchors.top : cuerpo.bottom
        anchors.topMargin : units.gu(2)
        anchors.horizontalCenter : parent.horizontalCenter
        width : parent.width * 0.5
        fillMode : Image.PreserveAspectFit
        source: '../assets/wifi-resaltado.png'
      }
      Text {
        id : 'cuerpo2'
        anchors.top : recorte1.bottom
        anchors.topMargin : units.gu(2)
        anchors.left : parent.left
        anchors.leftMargin : units.gu(2)
        anchors.right : parent.right
        anchors.rightMargin : units.gu(2)
        font.pixelSize: FontUtils.sizeToPixels("medium")
        wrapMode: Text.WordWrap
        width : parent.width
        horizontalAlignment : Text.AlignLeft
        font.family: "Ubuntu"
        textFormat: Text.RichText
        onLinkActivated: Qt.openUrlExternally(link)
        text: texto2
      }
      Image {
        id : 'recorte2'
        anchors.top : cuerpo2.bottom
        anchors.topMargin : units.gu(2)
        anchors.horizontalCenter : parent.horizontalCenter
        width : parent.width * 0.5
        fillMode : Image.PreserveAspectFit
        source: '../assets/update-resaltado.png'
      }
    }
    Button {
      id: "cerrar"
      text: ventanaEmergente.textoBoton
      anchors.bottom : parent.bottom
      anchors.bottomMargin : units.gu(2)
      height : units.gu(6)
      width : units.gu(15)
      anchors.horizontalCenter : parent.horizontalCenter
      onClicked: close()
    }

}
