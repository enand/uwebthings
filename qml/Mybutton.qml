/*
 * Copyright (C) 2021  ehf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * MuhfazaCoin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 import Ubuntu.Components 1.3
 import QtQuick 2.7
 import QtQuick.Controls 2.2

 Button {
   id: start
   property string textButton:""
   property bool enabledButton:true

   enabled: enabledButton
   width : parent.width * 0.8
   height : parent.height * 0.8
   anchors.horizontalCenter: parent.horizontalCenter
   anchors.verticalCenter:parent.verticalCenter
   text:i18n.tr(start.textButton)
   contentItem: Text {
     text: start.text
     font: start.font
     opacity: enabled ? 1.0 : 0.3
     color: 'white'
     horizontalAlignment: Text.AlignHCenter
     verticalAlignment: Text.AlignVCenter
     elide: Text.ElideRight
   }

   background: Rectangle {
     implicitWidth: parent.width * 0.8
     implicitHeight: parent.height * 0.8
     opacity: enabled ? 1 : 0.3
     color:'grey'
     border.color: 'black'
     border.width: 2
     radius: units.gu(3)
   }
 }
