/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Page {
    id: configuracion
    visible: false
    property var estadoConexion : ""
    property var existeConfiguracion : ''
    property string msjAyuda : i18n.tr('<p style="text-indent: 75px; margin-bottom: 50px;" ><b>IP: </b>Debes especificar la ip local donde tienes instalado el Gateway de Mozilla. </p><p style="text-indent: 75px; margin-bottom: 50px;" ><b>Dominio: </b>Escribe el dominio que elegiste al configurar por primera vez el Gateway, servirá para acceder al Gateway de la forma: <i>dominio.webthings.io (si tienes activa la pasarela)</i> a traves de internet.</p><p style="text-indent: 75px; margin-bottom:50px;" ><b>Usuario: </b> El nombre de usuario para acceder al Gateway, normalmente sera un email <b>(La app no guarda tu correo en ningún sitio)</b></p><p style="text-indent: 75px; margin-bottom: 50px;" ><b>Contraseña: </b>La contraseña para poder acceder junto con tu nombre de usuario.</p><p style="text-indent: 75px; margin-bottom: 50px;" ><b>Esta app no guarda ni tú contraseña ni tú nombre de usuario</b> estos datos se utilizan sólamente la primera vez en la primera conexión y después son olvidados, obteniendo si todo va bien un JWT (JSON Web Token) que devuelve el gateway. El JWT, será el unico dato almacenado junto con la IP local y el dominio.</p>')

    Emergente{
      id : 'ayuda'
      textoTitulo: i18n.tr("Ayuda")
      texto:i18n.tr(msjAyuda)
      textoBoton: i18n.tr("Cerrar")
      modal : true
    }
    EmergenteConfiguracion{
      id : 'netState'
      modal : true
      runActivities : true

    }
    header: PageHeader {
        id: cabeceraConfiguracion
        title: i18n.tr('Configuración')
        trailingActionBar{
            actions: [
                Action {
                    iconName: "go-home"
                    text: i18n.tr('Webthings')
                    onTriggered: pageStack.push(paginaInicial)
                },
                Action {
                    iconName: "help"
                    text: i18n.tr('Información')
                    onTriggered: ayuda.visible = true
                }
            ]
        }
    }
    Flickable{
      id : 'flickConfiguracion'
      clip: true
      width : parent.width
      contentHeight : formularioConfiguracion.height + units.gu(5)
      anchors.left:parent.left
      anchors.right:parent.right
      anchors.top : cabeceraConfiguracion.bottom
      anchors.topMargin: units.gu(2)
      anchors.bottom : lineaBotones.top

      Column {
        id : 'formularioConfiguracion'
        width: parent.width
        anchors.top:parent.top
        //anchors.topMargin : units.gu(2)
        //height : configuracion.height
        spacing : units.gu(2)

        Campo {
            id: "ip"
            nombre: "ip"
            height : units.gu(6)
            width: parent.width
            message: "Dirección IP (Red local)"
        }
        Campo {
            id: "dominio"
            nombre: "dominio"
            height : units.gu(6)
            width: parent.width
            message: "Pasarela remota (Url completa)"

        }
        Campo {
            id: "email"
            nombre: "email"
            height : units.gu(6)
            width: parent.width
            message: "Usuario (Email)"
            mail : true
        }
        Campo{
            id: "contrasena"
            nombre: "contrasena"
            height : units.gu(6)
            width: parent.width
            message: "Contraseña"
            password : true
        }
        Text{
          id:'textExisteConfiguracion'
          font.pixelSize: FontUtils.sizeToPixels("medium")
          wrapMode: Text.WordWrap
          width : parent.width * 0.9
          height : units.gu(20)
          anchors.horizontalCenter : parent.horizontalCenter
          horizontalAlignment : Text.AlignHCenter
          verticalAlignment : Text.AlignVCenter
          font.family: "Ubuntu"
          textFormat: Text.RichText
          visible : {if(existeConfiguracion != ''){true} else{false}}
          text:{
            if(existeConfiguracion == 2){i18n.tr('La configuración guardada es correcta, está configurada correctamente para usar la pasarela tanto en la red local como en remoto')}
            else {
              if(existeConfiguracion == 0){i18n.tr('La configuración guardada es correcta, está configurada únicamente para la red local')}
              else {
                if(existeConfiguracion == 1){i18n.tr('La configuración guardada es correcta, está configurada únicamente utilizar la pasarela remota')}
                else {''}
              }
            }
          }
        }

      }
    }
    Rectangle{
        id:'lineaBotones'
        width : parent.width
        height : units.gu(8)
        anchors.bottom : parent.bottom
        color : '#a5c1cc'
        Rectangle{
          id : 'recGuardar'
          anchors.left : parent.left
          width : parent.width * 0.5
          height : parent.height
          color : 'transparent'
          Button {
              id: "guardar"
              anchors.centerIn : parent
              text: i18n.tr("Guardar")

              enabled: {
                  //Activamos el boton cuando ha introducido valores
                  if (ip.valorEscrito.length > 0 && email.valorEscrito.length > 0 && contrasena.valorEscrito.length > 0)
                      return true
                  else
                      return false
              }
              onClicked: {
                netState.visible = true
                // Obtenemos la autorizacion JWT y Guardamos la configuración en el archivo
                python.call('PaginaInicial.obtenerJwt', [ ip.valorEscrito, dominio.valorEscrito, email.valorEscrito, contrasena.valorEscrito, root.applicationName], function(returnValue) {
                  estadoConexion = returnValue
                  if (estadoConexion == 'ERROR') {root.conexion = ''; netState.msj = 'No ha sido posible conectar con el servidor, por favor comprueba la configuracion.'}
                  if (estadoConexion == 2){root.conexion = 'both';netState.msj = '¡Perfecto!, tenemos conexión tanto en local como en remoto.'}
                  if (estadoConexion == 0){root.conexion = 'local';netState.msj = 'Correcto, hay conexión local al Webthings Gateway'}
                  if (estadoConexion == 1){root.conexion = 'remote';netState.msj = 'Hay conexion remota al servidor, usar esta opción siempre podria suponer consumo de datos'}
                  netState.runActivities = false
                });
              }
          }
        }
        Rectangle{
          id : 'recAyuda'
          anchors.left : recGuardar.right
          width : parent.width * 0.5
          height : parent.height
          color : 'transparent'
          Button {
            id: "botonAyuda"
            anchors.centerIn : parent
            text: i18n.tr("Ayuda")
            onClicked: {
              ayuda.visible = true
            }
          }
        }
    }
    // Comprobamos si la configuración existe para presentarla
    Component.onCompleted: {
        // cargamos el msj de Ayuda
        //python.call('PaginaInicial.leerTexto', [root.appFolder.toString(), 'ayuda.txt'], function(resultado){msjAyuda = resultado});
        python.call('PaginaInicial.comprobar', [root.applicationName], function(returnValue) {
            if (returnValue == true) {
                python.call('PaginaInicial.leerJwt', [root.applicationName], function(datos) {
                  existeConfiguracion = datos['net']
                  ip.valorEscrito = datos['ip'];
                  dominio.valorEscrito = datos['dominio']

                });
            }
        });
    }

}
