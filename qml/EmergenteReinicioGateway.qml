/*
 * Copyright (C) 2021  ehf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * MuhfazaCoin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Ubuntu.Components 1.3
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls 2.12


Popup {
    id: dialog
    property var contador : 0
    width: parent.width * 0.8
    height: parent.height * 0.6
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    background: Rectangle{
      width:parent.width
      radius:units.gu(1)
      color:"white"
      border.color: 'black'
    }
    property string msj:''
    property bool runActivities : true

    Flickable {
      id:flickableDialog
      clip: true
      anchors.left:parent.left
      anchors.right:parent.right
      anchors.top:parent.top
      anchors.bottom:pieEmergente.top
      bottomMargin: units.gu(2)
      contentHeight: textTitulo.height + textExplicacion.height + units.gu(5)
      Text{
        id:textTitulo
        anchors.top : parent.top
        anchors.topMargin : units.gu(2)
        font.pixelSize: FontUtils.sizeToPixels("large")
        wrapMode: Text.WordWrap
        width : parent.width
        horizontalAlignment : Text.AlignHCenter
        font.family: "Ubuntu"
        textFormat: Text.RichText
        text:i18n.tr('Reinicio del Gateway')
      }
      Icon {
         id : 'iconoAdvertencia'
         width: units.gu(8)
         height: units.gu(8)
         anchors.top : textTitulo.bottom
         anchors.topMargin : units.gu(2)
         anchors.horizontalCenter : parent.horizontalCenter
         //visible :
         color : 'red'
         name: 'security-alert'
       }
      Text{
        id:textExplicacion
        anchors.top : iconoAdvertencia.bottom
        anchors.topMargin : units.gu(2)
        font.pixelSize: FontUtils.sizeToPixels("medium")
        wrapMode: Text.WordWrap
        width : parent.width
        height : units.gu(12)
        horizontalAlignment : Text.AlignHCenter
        verticalAlignment : Text.AlignVCenter
        //color:g.colorTextoEmergente
        font.family: "Ubuntu"
        textFormat: Text.RichText
        text:i18n.tr('Vas a reiniciar el gateway, se perderá la conexión. La app volverá a intentar conectarse pasado un minuto, puedes esperar o cerrar la app y volver a abrirla cuando quieras.')
      }
      Timer{
        id: 'minuto'
        interval : 60000
        onTriggered : {
          segundo.running = false
          activarSocket = false
          socket.active = false
          pageStack.push(paginaInicial)
          close()
        }
      }
      Timer{
        id : 'segundo'
        repeat : true
        interval : 1000
        onTriggered :{ contador = contador +1}
      }
      ProgressBar {
        id: 'miBarra'
        value: contador
        padding: units.gu(2)
        width : parent.width * 0.9
        anchors.centerIn : parent
        visible : false
        from : 0
        to : 60

        background: Rectangle {
          implicitWidth: parent.width * 0.8
          implicitHeight: units.gu(6)
          color: "#f1f1f1"
          border.color : '#a5c1cc'
          radius: units.gu(2)
        }
        contentItem: Item {
            implicitWidth: parent.width * 0.8
            implicitHeight: units.gu(0.75)
            Rectangle {
                width: miBarra.visualPosition * parent.width
                height: parent.height
                radius: units.gu(0.3)
                color: '#a5c1cc'
            }
        }

      }
    }
    Row{
      id:pieEmergente
      anchors.bottom:parent.bottom
      width:parent.width
      height:units.gu(4)
      Rectangle{
        id: recCancelar
        radius : units.gu(1)
        width:parent.width * 0.5
        height:parent.height
        color:"transparent"
        Mybutton{
          id:cancelarButton
          textButton: i18n.tr("Cancelar")
          onClicked:{
            close()
          }
        }
      }
      Rectangle{
        id:recReiniciar
        radius : units.gu(1)
        width:parent.width * 0.5
        height:parent.height
        color:"transparent"
        Mybutton{
          id:reiniciarButton
          textButton:i18n.tr("Reiniciar Gateway")
          onClicked:{
            python.call('PaginaInicial.reiniciarGateway', [root.jwtValido, root.wifi, root.ip, root.dominio], function(resultado){
              minuto.running = true
              segundo.running = true
              miBarra.visible = true
            });

          }
        }
      }
    }
}
