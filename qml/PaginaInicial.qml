/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtWebSockets 1.0

Page {
    id: showThings
    // Añado la propiedad para no tener que leer el archivo siempre

    property var datos:[]
    property var ordenDevices : {}
    property var thingConectado : []
    property var resultSocket : {}
    property bool activarSocket : false

    //Configuro los datos de la ventan emergente
    property Component info: Emergente{
        textoTitulo: i18n.tr("Todavía nada que mostrar")
        texto:i18n.tr("<p>Todavía no tenemos la autorización del Gateway, para mostrar ningún elemento.</p><p>Debes revisar la configuración y comprobar que todos los datos son correctos</p>")
        textoBoton: i18n.tr("Cerrar")
    }
    EmergenteReinicioGateway{
      id : 'reinicioGateway'
      modal : true
    }
    EmergenteHelp{
      id : 'emergenteHelp'
      modal : true
    }

    ToolTip {
      id: 'msjCorto'
      timeout: 5000
      anchors.centerIn : parent
      width : parent.width * 0.5
      height : units.gu(12)
      contentItem: Text {
          id : 'textToolTip'
          text: msjCorto.text
          width : parent.width
          height : parent.height
          font.pixelSize: FontUtils.sizeToPixels("medium")
          horizontalAlignment : Text.AlignHCenter
          verticalAlignment : Text.AlignVCenter
          clip : true
          wrapMode : Text.WordWrap
          color: "#000000"
      }
      background: Rectangle {
          id : 'recToolTip'
          color: "#e9e9e9"
          width : parent.width
          height :parent.height
          radius : units.gu(2)
          border.color : '#086da6'
      }
    }
    header: PageHeader {
        id: header
        title: i18n.tr('Webthings')
        leadingActionBar {
            actions: [
              Action {
                  iconName: "reset"
                  text: i18n.tr('Reinicio Gateway')
                  onTriggered: {
                    reinicioGateway.visible = true
                  }
              },
              Action {
                  iconName: "view-list-symbolic"
                  text: i18n.tr('Reglas')
                  onTriggered: {
                    //Detengo el socket
                    activarSocket = false
                    socket.active = false
                    pageStack.push(paginaReglas)
                  }
              },
              Action {
                  iconName: "settings"
                  text: i18n.tr('Configuración')
                  onTriggered: {
                    //Detengo el socket
                    activarSocket = false
                    socket.active = false
                    pageStack.push(paginaConfiguracion)
                  }
              },
              Action {
                  iconName: "info"
                  text: i18n.tr('Información')
                  onTriggered: {
                    //Detengo el socket
                    activarSocket = false
                    socket.active = false
                    pageStack.push(paginaInfo)
                  }
              }
            ]
        }
        trailingActionBar {

          actions: [
              Action {
                  id : 'botonRed'
                  checkable : true
                  iconName: {
                    if (root.errorConexion){
                      if (root.wifi){'preferences-network-wifi-no-connection-symbolic'}
                      else {'transfer-error'}
                    }
                    else {
                      if (root.wifi){"preferences-network-wifi-active-symbolic"}
                      else {'transfer-progress'}
                    }
                  }
                  onToggled:{
                    // Comprobamos que tipo de conexion tenemos configurada
                    if (root.conexion == 'both'){
                      if (root.wifi) {
                        root.wifi = false
                        root.jwtValido = root.jwtRemote
                        msjCorto.text = i18n.tr("Conectado al Gateway a través de la pasarela remota, puede generar gasto de datos")
                        msjCorto.visible = true
                      }
                      else {
                        root.wifi = true
                        root.jwtValido = root.jwt
                        msjCorto.text = i18n.tr("Conectado al Gateway a través de la red local")
                        msjCorto.visible = true
                      }
                    }
                    if (root.conexion == 'local'){
                      msjCorto.text = i18n.tr("Conectado al Gateway a través de la red local, la conexion remota NO ESTÁ CONFIGURADA")
                      msjCorto.visible = true
                    }
                    if (root.conexion == 'remote'){
                      msjCorto.text = i18n.tr("Conectado al Gateway a través de la pasarela remota, la conexión local NO ESTÁ CONFIGURADA. Con esta configuración se pueden producir consumo de datos.")
                      msjCorto.visible = true
                    }
                  }
              },
              Action {
                  id : 'botonActualizar'
                  //checkable : true
                  iconName: 'sync'
                  onTriggered:{
                    botonActualizar.checked = false
                    msjCorto.text = i18n.tr("Listado de dispositivos actualizado correctamente.")
                    msjCorto.visible = true
                    // Primero actualizo el fichero things.json y despues recargo los datos
                    python.call('PaginaInicial.guardarThingsGateway', [root.jwtValido, root.wifi, root.ip, root.dominio, root.applicationName], function(resultado){
                      python.call('PaginaInicial.readThings', [root.jwtValido, root.wifi, root.ip, root.dominio, root.applicationName, root.appFolder.toString()], function(resultado){
                        // Meto el Json devuelto en una variable
                        datos = resultado[0]
                        ordenDevices = resultado[1]
                      });
                    });

                  }
              },
              Action {
                  id : 'botonHelp'
                  //checkable : true
                  iconName: 'help'
                  onTriggered:{emergenteHelp.visible = true}
              }
         ]
         numberOfSlots: 3
      }
    }
    Column {
        id:"things"
        anchors.top : header.bottom
        anchors.topMargin : units.gu(2)
        height: parent.height
        width: parent.width
        enabled:false
        onEnabledChanged:{
            if (things.enabled == true){
                python.call('PaginaInicial.readThings', [root.jwtValido, root.wifi, root.ip, root.dominio, root.applicationName, root.appFolder.toString()], function(resultado){
                    // Meto el Json devuelto en una variable
                    datos = resultado[0]
                    ordenDevices = resultado[1]
                    activarSocket = true
                });
            }
        }

        WebSocket {
            id: socket
            url: {
              if (root.wifi){'ws://' + root.ip + ':8080/things' + '?jwt=' + root.jwtValido}
              else {'wss://' + root.dominioLimpio + '/things' + '?jwt=' + root.jwtValido}
            }
            property var indice : 0
            onTextMessageReceived: {
              resultSocket = JSON.parse(message)
              //Miramos si está conectado

              if (resultSocket.messageType == 'connected'){
                // Modificamos el array con los things en python para marcarlo como conectado/desconectaddo
                indice = ordenDevices[resultSocket.id]
                datos[indice]['conectado'] = resultSocket.data
                //console.log("\nReceived message: " + message)
              }
              else if(resultSocket.messageType == 'propertyStatus'){
                if ('on' in resultSocket.data){
                  indice = ordenDevices[resultSocket.id]
                  datos[indice]['encendido'] = resultSocket.data.on
                  //console.log("\nReceived message: " + message)
                }
                else {
                  if ('on1' in resultSocket.data){
                    indice = ordenDevices[resultSocket.id]
                    datos[indice]['encendido'] = resultSocket.data.on1
                    //console.log("\nReceived message: " + message)
                  }
                }
              }
              showThings.datos = datos
            }
            onStatusChanged: if (socket.status == WebSocket.Error) {
                                root.errorConexion = true
                                 console.log("Error: " + socket.errorString)
                             } else if (socket.status == WebSocket.Open) {
                                root.errorConexion = false
                                 console.log('Socket opened +++++++++')
                             } else if (socket.status == WebSocket.Closed) {
                                root.errorConexion = true
                                 console.log('Socket closed*******************')
                             }
            active: activarSocket ? true:false
        }
        MostrarCosas {}

    }
    //Compruebo si existe el JWT, si no es así informo
    Component.onCompleted: {
        if (root.jwtValido == ""){
            // Si todavía no tengo el jwt en memoria hago las comprobaciones
            python.call('PaginaInicial.comprobar', [root.applicationName], function(returnValue) {
                if (returnValue == true) {
                    //Si tenemos JWT lo leemos y tambien leemos la ip y el dominio
                    python.call('PaginaInicial.leerJwt', [root.applicationName], function(datos) {
                      if(datos['net'] == 0){
                        root.jwtValido = datos['jwt']
                        root.jwt = datos['jwt']
                        root.jwtRemote = ''
                        root.conexion = 'local'
                        root.wifi = true
                        root.ip = datos['ip']
                        root.dominio = datos['dominio']
                        root.dominioLimpio = datos['dominiolimpio']
                        things.enabled = true
                      }
                      if(datos['net'] == 1){
                        root.jwtValido = datos['jwtremote']
                        root.jwt = ''
                        root.jwtRemote = datos['jwtremote']
                        root.conexion = 'remote'
                        root.wifi = false
                        root.ip = datos['ip']
                        root.dominio = datos['dominio']
                        root.dominioLimpio = datos['dominiolimpio']
                        things.enabled = true
                      }
                      if(datos['net'] == 2){
                        root.jwtValido = datos['jwt']
                        root.jwt = datos['jwt']
                        root.jwtRemote = datos['jwtremote']
                        root.conexion = 'both'
                        root.wifi = true
                        root.ip = datos['ip']
                        root.dominio = datos['dominio']
                        root.dominioLimpio = datos['dominiolimpio']
                        things.enabled = true

                      }

                    });


                }
                else {PopupUtils.open(info)};
            });
        }
        else{things.enabled = true}
    }
}
