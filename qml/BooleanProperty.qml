/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 import QtQuick 2.12
 import Ubuntu.Components 1.3
 import QtQuick.Controls 2.2
 
 Rectangle{
   width : parent.width
   height : parent.height
   anchors.centerIn : parent
   color : 'transparent'
   //color : 'lightblue'
   //opacity : 0.6
   //border.color : 'black'
   Icon {
      width: units.gu(3)
      height: units.gu(3)
      anchors.centerIn : parent
      color : listaPropiedades[index][1]['value'] ? 'green' : 'red'
      name: {
        if (listaPropiedades[index][1]['value']){'tick'}
        else{'edit-clear'}
      }
  }
}
