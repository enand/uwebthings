/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.7
import QtQuick.Controls 2.2



Popup {
    id: ventanaEmergente
    width: parent.width * 0.8
    height: parent.height * 0.8
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    background: Rectangle{
      width:parent.width
      radius:units.gu(1)
      color:"Lightblue"
    }
    property string textoTitulo:""
    property string texto:""
    property string textoBoton:""

    Flickable {
      id:flickableIntro
      clip: true
      anchors.left:parent.left
      anchors.right:parent.right
      anchors.top:parent.top
      anchors.topMargin : units.gu(2)
      anchors.bottom:cerrar.top
      bottomMargin: units.gu(2)
      contentHeight: icono.height + titulo.height + cuerpo.height + units.gu(10)
      Image {
        id : 'icono'
        anchors.top : parent.top
        anchors.topMargin : units.gu(0.5)
        anchors.horizontalCenter : parent.horizontalCenter
        width : units.gu(9)
        height : units.gu(9)
        source: '../assets/logo.png'
      }
      Text{
        id: "titulo"
        anchors.top : icono.bottom
        anchors.topMargin : units.gu(2)
        anchors.left : parent.left
        anchors.leftMargin : units.gu(2)
        anchors.right : parent.right
        anchors.rightMargin : units.gu(2)
        font.pixelSize: FontUtils.sizeToPixels("large")
        wrapMode: Text.WordWrap
        width : parent.width
        horizontalAlignment : Text.AlignHCenter
        font.family: "Ubuntu"
        textFormat: Text.RichText
        text: ventanaEmergente.textoTitulo
      }
      Text {
        id : 'cuerpo'
        anchors.top : titulo.bottom
        anchors.topMargin : units.gu(2)
        anchors.left : parent.left
        anchors.leftMargin : units.gu(2)
        anchors.right : parent.right
        anchors.rightMargin : units.gu(2)
        font.pixelSize: FontUtils.sizeToPixels("medium")
        wrapMode: Text.WordWrap
        width : parent.width
        horizontalAlignment : Text.AlignLeft
        font.family: "Ubuntu"
        textFormat: Text.RichText
        onLinkActivated: Qt.openUrlExternally(link)
        text: ventanaEmergente.texto
      }
    }
    Button {
      id: "cerrar"
      text: ventanaEmergente.textoBoton
      anchors.bottom : parent.bottom
      anchors.bottomMargin : units.gu(2)
      height : units.gu(6)
      width : units.gu(15)
      anchors.horizontalCenter : parent.horizontalCenter
      onClicked: close()
    }

}
