/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 import QtQuick 2.12
 import Ubuntu.Components 1.3
 import QtQuick.Controls 2.2
 import QtQuick.Dialogs 1.0

Rectangle {
  id : 'casillaPropiedad'
  width : parent.width
  height : parent.height
  anchors.centerIn : parent
  color : 'transparent'

  /*ColorDialog {
    id: colorDialog
    title: "Please choose a color"
    showAlphaChannel : false

    onAccepted: {
        console.log(colorDialog.color)
        socketThing.sendTextMessage(msjEnvioSocket)
        colorDialog.close()
    }
    onRejected: {
        colorDialog.close()
    }
  }*/
  Button {
    id: control
    anchors.centerIn : parent
    property var valor : 0
    enabled : listaPropiedades[index][1]['readOnly'] ? false : true
    text : i18n.tr('Color')
    onClicked: {
      //colorDialog.color = listaPropiedades[index][1]['value']
      //colorDialog.visible = true
      emergenteColor.visible = true
      emergenteColor.indicePropiedad = index
    }
    contentItem: Text {
        text: control.text
        font: control.font
        opacity: enabled ? 1.0 : 0.3
        color: control.down ? "#17a81a" : "#21be2b"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitWidth: casillaPropiedad.width * 0.5
        implicitHeight: units.gu(3)
        opacity: enabled ? 1 : 0.3
        border.color: control.down ? "#17a81a" : "#21be2b"
        border.width: 1
        radius: 2
    }

  }
  Icon {
     id : 'muestraColor'
     anchors.top : control.bottom
     anchors.topMargin : units.gu(1)
     anchors.horizontalCenter : parent.horizontalCenter
     width: units.gu(3)
     height: units.gu(3)
     visible : true
     color : listaPropiedades[index][1]['value']
     name:  'preferences-color-symbolic'
  }
}
