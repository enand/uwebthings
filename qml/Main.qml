/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import io.thp.pyotherside 1.5
import Ubuntu.Components.Popups 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'uwebthings.enand'
    automaticOrientation: true
    width: units.gu(45)
    height: units.gu(75)
    anchorToKeyboard: true

    property var appFolder : Qt.resolvedUrl("..")
    property string idSelectThing : ''
    property string msjBienvenida : ''
    property string jwt:""
    property string jwtRemote : ""
    property string jwtValido : ""
    property string ip:""
    property string dominio:""
    property string dominioLimpio:""
    property string conexion : ""
    property string iconoActivo : ''
    property string titleCosaActiva : ''
    property string idCosaActiva : ''
    property bool wifi : true
    property bool errorConexion : false

    PageStack {
        id: pageStack
        anchors.fill: parent

        //Compruebo si está el JWT, de otra forma informo y envio a la pagina de configuracion
        Component.onCompleted: {
            python.call('PaginaInicial.comprobar', [root.applicationName], function(returnValue) {
                if (returnValue == true) {
                    push(paginaInicial)
                }
                else {
                  python.call('PaginaInicial.leerTexto', [root.appFolder.toString(), 'intro.txt'], function(resultado){msjBienvenida = resultado});
                  miEmergente.visible = true
                  push(paginaConfiguracion)
                };
            });
        }
        // Configuro la información de la ventan emergente
        Emergente{
          id:miEmergente
          modal:true
          textoTitulo: i18n.tr("Bienvenid@")
          texto:i18n.tr(msjBienvenida)
          textoBoton: i18n.tr("Empezar")
        }

        Component {
            id: paginaInicial
            PaginaInicial {}
        }
        Component {
            id: paginaInformacion
            PaginaInformacion {}
        }
        Component {
            id: paginaConfiguracion
            PaginaConfiguracion {}
        }
        Component {
            id: paginaShowthing
            ShowThing {}
        }
        Component {
            id: paginaReglas
            ShowRules {}
        }
        Component {
            id: paginaInfo
            PaginaInformacion {}
        }
    }
    //Cargo el archivo de script python
    Python {
        id: python
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));
            importModule('PaginaInicial', function() {});
            //importModule('CallSockets', function() {});
        }
        onError: {
            console.log('python error: ' + traceback);
        }
    }

}
