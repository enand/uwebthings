/*
 * Copyright (C) 2020  Enri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * webthings is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 import QtQuick 2.12
 import Ubuntu.Components 1.3
 import QtQuick.Controls 2.2
Rectangle {
  id : 'casillaPropiedad'
  width : parent.width
  height : parent.height
  anchors.centerIn : parent
  color : 'transparent'
  property var msjEnvioSocket : '{\"messageType\": \"setProperty\", \"data\" :{\"' + listaPropiedades[index][1]['name'] + '\":\"' + control. currentText + '\"}}'

  ComboBox {
    id: control
    anchors.centerIn : parent
    editable: false
    property var valor : 0
    enabled : listaPropiedades[index][1]['readOnly'] ? false : true
    model : listaPropiedades[index][1]['enum']
    currentIndex : listaPropiedades[index][1]['enum'].indexOf(listaPropiedades[index][1]['value'])
    contentItem: Text {
        leftPadding: 0
        rightPadding: control.indicator.width + control.spacing
        text: control.displayText
        font: control.font
        color: control.pressed ? "#17a81a" : "#21be2b"
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitWidth: casillaPropiedad.width * 0.8
        implicitHeight : units.gu(3)
        border.color: control.pressed ? "#17a81a" : "#21be2b"
        border.width: control.visualFocus ? 2 : 1
        radius: 2
    }
    onActivated:{
      //console.log(msjEnvioSocket)
      socketThing.sendTextMessage(msjEnvioSocket)
      // Detengo el socket puesto que al volver a cargar la pagina se vuelve a abrir
      pageStack.clear()
      socketThing.active = false
      pageStack.push(paginaShowthing)
    }
  }
}
