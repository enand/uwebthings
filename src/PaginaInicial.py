'''
 Copyright (C) 2020  Enri

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 webthings is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import os.path as path
import re
from os.path import expanduser
import os
import requests
import json
import pprint
import collections

# Funcion que comprueba la existencia del archivo de configuracion
def comprobar(nombreApp):
    # Comprobamos si tenemos el archivo de configuracion de no ser así es la primera vez
    home = expanduser("~")
    ruta = home + '/.config/' + nombreApp +'/' + nombreApp + '.json'
    return path.exists(ruta)

# Función que valida el campo ip y no permite campos vacios en el resto
def validarCampo(campo, valorEscrito):
    #print(campo)
    #print(valorEscrito)
    if len(valorEscrito) > 0:
        if campo == "ip":
            m = re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", valorEscrito)
            return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))

        else:
            return True
    else:
        if campo == 'dominio':
            #El dominio permitimos que se quede en blanco
            return True
        else:
            return False

# Funcion que con los datos introducidos, obtiene el JWT de webthings y lo guarda, ademas de guardar el archivo de
# configuracion.
# Las rutas son: archivo de configuracion --> /home/.config/(Appname)/(Appname).json
# Ademas devuelve a qml los datos de configuracion

def obtenerJwt (ip, dominio, usuario, contrasena, nombreApp):
    configuracion = {}
    local = False
    remote = False
    configuracion['jwt'] = ''
    configuracion['jwtremote'] = ''
    home = expanduser("~")

    #Hacemos la llamada al gateway para que nos devuelva el jwt
    cabeceras = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    datos = {'email': usuario, 'password':contrasena}
    url = 'http://' + ip + ':8080/login'
    # Comprobamos si el usuario ha escrito un dominio
    if dominio != '':
        urlRemote = dominio + '/login'

    # Obtenemos el jwt local
    resultado_1 = ''
    try:
        responseLocal = requests.post(url, json=datos, headers=cabeceras, verify=False)
    except:
        resultado_1 = 'ERROR'
    if resultado_1 != 'ERROR':
        if responseLocal.ok:
            token = responseLocal.json()
            configuracion['jwt'] = token['jwt']
            local = True

    # Si el usuario ha escrito un dominio lo intentamos
    resultado_1 = ''
    if dominio != '':
        # Obtenemos el jwt remoto
        try:
            responseRemote = requests.post(urlRemote, json=datos, headers=cabeceras, verify=False)
        except:
            resultado_1 = 'ERROR'
        if resultado_1 != 'ERROR':
            if responseRemote.ok:
                tokenRemote = responseRemote.json()
                configuracion['jwtremote'] = tokenRemote['jwt']
                remote = True

    if not(local) and not(remote):
        resultado = 'ERROR'
        return resultado
    elif local and remote:
        configuracion['net'] = 2
    elif local:
        configuracion['net'] = 0
    else:
        configuracion['net'] = 1
    # Ruta para guardar el archivo de configuracion con los jwt
    rutaConfiguracionFolder = home + '/.config/' + nombreApp +'/'
    rutaConfiguracion = home + '/.config/' + nombreApp +'/' + nombreApp + '.json'
    # Si no existe la ruta la creamos
    os.makedirs(rutaConfiguracionFolder, exist_ok=True)
    configuracion['ip'] = ip
    configuracion['dominio'] = dominio
    #Quito el http y https, para usar luego en el socket
    dominioLimpio = dominio.replace('http://','')
    dominioLimpio = dominio.replace('https://','')
    configuracion['dominiolimpio'] = dominioLimpio
    # Guardamos el archivo de configuracion y devolvemos el estado de la operacion
    with open(rutaConfiguracion, 'w') as thingsJsonFile:
        json.dump(configuracion, thingsJsonFile, indent=4)
    # El resultado es 0=local, 1=remoto, 2=ambos
    resultado = configuracion['net']
    return resultado

# Funcion para leer el archivo de configuracion y obtener los datos
def leerJwt(nombreApp):
    home = expanduser("~")
    rutaConfiguracion = home + '/.config/' + nombreApp +'/' + nombreApp + '.json'
    with open(rutaConfiguracion) as thingsJson:
        data = json.load(thingsJson)
    return data


# Funcion para obtener todas las cosas del gateway y guardarlas en el fichero things.json
def guardarThingsGateway(jwt, wifi, ip, dominio, nombreApp):
    home = expanduser("~")
    ruta = home + '/.local/share/' + nombreApp +'/things.json'
    rutaFolder = home + '/.local/share/' + nombreApp +'/'
    cabecera = {'Authorization':'Bearer '+jwt, 'Content-Type': 'application/json', 'Accept': 'application/json'}
    # Si no existe la ruta la creamos
    os.makedirs(rutaFolder, exist_ok=True)
    #Comprobamos si nos conectamos a local o remoto
    if wifi:
        url = 'http://'+ip+':8080/things'
        textoErrorConexion ='Error conectando al servidor local'
    else:
        url = dominio + '/things'
        textoErrorConexion ='Error conectando a la pasarela en internet'
    try:
        valor = requests.get(url, headers=cabecera)
        valor.raise_for_status()
    except requests.exceptions.ConnectionError:
        return 'ERROR', textoErrorConexion
    except:
        return 'ERROR', 'Ha ocurrido un error'
    # Guardamos los datos en el fichero json
    data = valor.json()
    with open(ruta, 'w') as thingsJsonFile:
        json.dump(data, thingsJsonFile, indent=4)

## Funcion para leer las cosas del gateway del archivo things.json y devuelve lo mas importante
def readThings(jwt, wifi, ip, dominio, nombreApp, appFolder):
    # La ruta base que nos da QML lleva el prefijo file:// se lo tenemos que quitar
    # Esta es la ruta base de la app
    appFolder = appFolder.replace('file://','')
    # Obtenemos la lista de nombres de los iconos de los devices (archivos que hay en la carpeta)
    listaIconos = os.listdir(appFolder + 'assets/devices/')

    home = expanduser("~")
    ruta = home + '/.local/share/' + nombreApp +'/things.json'
    # Leemos el archivo y si no existe lo creamos
    if not(path.exists(ruta)):
        # Si no existe creamos el archivo things.json
        guardarThingsGateway(jwt, wifi, ip, dominio, nombreApp)
    # Leemos los datos del archivo
    with open(ruta) as thingsJson:
        data = json.load(thingsJson)

    # Vamos a devolver solo los datos necesarios y no toda la descripcion de todas las cosas
    dataFiltrado = []
    deviceFiltrado = {}
    for device in data:
        #Averiguo que tipo y le asigno el icono correspondiente, con la excepcion que si es un device
        # que muestra el tiempo, lo miramos en el titulo para saberlo, no hay otra forma
        if device['title'][0:7] == 'Weather' or device['title'][0:7] == 'weather':
            deviceFiltrado['icono'] = 'Weather.svg'
        else:
            if (device['selectedCapability'] + '.svg') in listaIconos:
                deviceFiltrado['icono'] = device['selectedCapability'] + '.svg'
            else:
                deviceFiltrado['icono'] = 'Other.svg'
        # Una vez asignado el icono, obtenemos los datos que necesitamos
        deviceFiltrado['selectedCapability'] = device['selectedCapability']
        deviceFiltrado['id'] = device['id']
        # Obtenemos el id individual
        posicionCorte = deviceFiltrado['id'].find('/things/')
        deviceFiltrado['idlimpio'] = deviceFiltrado['id'][posicionCorte+8:]
        deviceFiltrado['title'] = device['title']
        # por defecto, todos los dispositivos se marcan como conectados y apagados
        deviceFiltrado['conectado'] = True
        deviceFiltrado['encendido'] = False
        # Añado cada diccionario a la lista que devolveré
        dataFiltrado.append(deviceFiltrado)
        deviceFiltrado = {}
    # creamos un diccionario con el orden de las cosas
    ordenThings = {}
    i = 0
    for thing in dataFiltrado:
        ordenThings[thing['idlimpio']] = i
        i=i+1
    return dataFiltrado, ordenThings



## Funcion para leer las propiedades de una cosa
def readProperties(jwt, wifi, ip, dominio, idThing):
    #quitamos lo que no nos hace falta del id del thing
    posicionCorte = idThing.find('/things/')
    thingLimpio = idThing[posicionCorte:]
    cabecera = {'Authorization':'Bearer '+jwt, 'Content-Type': 'application/json', 'Accept': 'application/json'}
    #Comprobamos si nos conectamos a traves de wifi o de internet
    if wifi:
        url = 'http://'+ip+':8080' + thingLimpio
        url2 = url + '/properties'
        textoErrorConexion ='Error conectando al servidor local'
    else:
        url = dominio + thingLimpio
        url2 = url + '/properties'
        textoErrorConexion ='Error conectando a la pasarela en internet'

    try:
        definicionPropiedades = requests.get(url, headers=cabecera)
        valuesPropiedades = requests.get(url2, headers=cabecera)
        definicionPropiedades.raise_for_status()
        valuesPropiedades.raise_for_status()
    except requests.exceptions.ConnectionError:
        return 'ERROR', textoErrorConexion
    except:
        return 'ERROR', 'Ha ocurrido un error'

    definicionPropiedades = definicionPropiedades.json()
    definicionPropiedades = definicionPropiedades['properties']
    valuesPropiedades = valuesPropiedades.json()

    # Controlo las unidades de medida para poder presentarlas
    unidadesMedida = {"year" : "Y", "month" : "M", "minute" : "min", "hour" : "H", "degree" : "°", "hectopascal" : "hPa", "percent":"%", "kelvin":"°K", "ppm":"ppm", "ampere":"A", "micrograms per cubic metre":"mpCm", "hertz":"Hz", "watt":"W", "degree celsius": "°C", "volt":"V"}

    # Añado el valor de la propiedad al resultado
    for tituloPropiedad in definicionPropiedades:
        # Si existe la unidad de medida la cambio por el diminutivo
        if 'unit' in definicionPropiedades[tituloPropiedad]:
            unidadPropiedad = definicionPropiedades[tituloPropiedad]['unit']
            if unidadPropiedad in unidadesMedida:
                definicionPropiedades[tituloPropiedad]['unit'] = unidadesMedida[unidadPropiedad]
        else:
            definicionPropiedades[tituloPropiedad]['unit'] = ''

        # compruebo si la propiedad viene en los resultados de los valores, alguna no viene
        if tituloPropiedad in valuesPropiedades:
            # Si el resultado existe en los valores, lo cojo y lo meto en la definicion de propiedades, como value
            definicionPropiedades[tituloPropiedad]['value'] = valuesPropiedades[tituloPropiedad]

    # Ordeno el diccionario por las claves
    result = collections.OrderedDict(sorted(definicionPropiedades.items()))
    #Convierto el resultado en una lista para mandarlo a qml
    orden = {}
    i = 0
    for propiedad in result:
        orden[propiedad] = i
        i=i+1
    listaPropiedades = list(result.items())

    return listaPropiedades, orden

# Funcion para cambiar el valor de una propiedad de un thing
'''
def establecerValor(jwt, wifi, ip, dominio, idThing, valor, nombrePropiedad):
    print(idThing)
    print(valor)
    print(nombrePropiedad)
    cabecera = {'Authorization':'Bearer '+jwt, 'Content-Type': 'application/json', 'Accept': 'application/json'}
    infoJson = {nombrePropiedad : valor}
    #Comprobamos si nos conectamos a local o remoto
    if wifi:
        url = 'http://'+ip+':8080' + idThing
        textoErrorConexion ='Error conectando al servidor local'
    else:
        url = dominio + idThing
        textoErrorConexion ='Error conectando a la pasarela en internet'
    try:
        resultado = requests.put(url, data=json.dumps(infoJson), headers=cabecera)
        resultado.raise_for_status()
    except requests.exceptions.ConnectionError:
        return 'ERROR', textoErrorConexion
    except:
        return 'ERROR', 'Ha ocurrido un error'
    return
'''
# Funcion para reiniciar el gateway
def reiniciarGateway(jwt, wifi, ip, dominio):
    cabecera = {'Authorization':'Bearer '+jwt, 'Content-Type': 'application/json', 'Accept': 'application/json'}
    #Comprobamos si nos conectamos a local o remoto
    if wifi:
        url = 'http://'+ip+':8080/settings/system/actions'
        #url = 'http://' + ip + ':8080/rules'
        textoErrorConexion ='Error conectando al servidor local'
    else:
        url = dominio + '/settings/system/actions'
        textoErrorConexion ='Error conectando a la pasarela en internet'

    msjReiniciar = {"action": "restartSystem"}
    try:
        #resultado = requests.get(url, headers=cabecera)
        resultado = requests.post(url, data = json.dumps(msjReiniciar) ,headers=cabecera)
        resultado.raise_for_status()
    except requests.exceptions.ConnectionError:
        print(requests.exceptions)
        return 'ERROR', textoErrorConexion
    except:
        print(requests.exceptions)
        return 'ERROR', 'Ha ocurrido un error'

    #print(resultado.json())
    return

# Funcion que lee las reglas de la pasarela y las muestra para poder activarlas / desactivarlas
def readRules(jwt, wifi, ip, dominio):
    cabecera = {'Authorization':'Bearer '+jwt, 'Content-Type': 'application/json', 'Accept': 'application/json'}
    #Comprobamos si nos conectamos a local o remoto
    if wifi:
        url = 'http://' + ip + ':8080/rules'
        textoErrorConexion ='Error conectando al servidor local'
    else:
        url = dominio + '/rules'
        textoErrorConexion ='Error conectando a la pasarela en internet'
    try:
        resultado = requests.get(url, headers=cabecera)
        #resultado = requests.post(url, data = msjReiniciar ,headers=cabecera)
        resultado.raise_for_status()
    except requests.exceptions.ConnectionError:
        return 'ERROR', textoErrorConexion
    except:
        return 'ERROR', 'Ha ocurrido un error'
    resultado = resultado.json()
    return resultado

#######################################################################################################
# Funcion para activar / desactivar una regla
# Funcion actualmente sin uso, no hay api-rest de webthings que nos permitan modificar las reglas
# está en propuesta, esperaremos a proximas versiones
# https://github.com/WebThingsIO/gateway/issues/2097
#######################################################################################################
'''
def establecerRegla(jwt, wifi, ip, dominio, idRegla, propiedad, valor):
    print(jwt)
    print(idRegla)
    print(propiedad)
    print(valor)
    cabecera = {'Authorization':'Bearer '+jwt, 'Content-Type': 'application/json', 'Accept': 'application/json'}
    infoJson = {propiedad : valor}
    print(infoJson)
    print(json.dumps(infoJson))
    #Comprobamos si nos conectamos a local o remoto
    if wifi:
        url = 'http://'+ip+':8080/rules/' + idRegla
        textoErrorConexion ='Error conectando al servidor local'
    else:
        url = dominio + '/rules/' + idRegla
        textoErrorConexion ='Error conectando a la pasarela en internet'
    try:
        resultado = requests.put(url, data=json.dumps(infoJson), headers=cabecera)
        resultado.raise_for_status()
    except requests.exceptions.ConnectionError:
        return 'ERROR', textoErrorConexion
    except:
        #print(requests.reason)
        print(resultado.status_code)
        return 'ERROR', 'Ha ocurrido un error'
    print(resultado.status_code)
    return
'''
# Funcion para leer un archivo de texto
'''
def leerTexto(appFolder, nombreArchivo):
    # La ruta base que nos da QML lleva el prefijo file:// se lo tenemos que quitar
    # Esta es la ruta base de la app
    appFolder = appFolder.replace('file://','')
    # Obtenemos la lista de nombres de los iconos de los devices (archivos que hay en la carpeta)
    archivoTxt = appFolder + 'assets/text/' + nombreArchivo
    with open(archivoTxt, 'r') as archivo:
        texto = archivo.read()
    return texto
'''
