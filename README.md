## UWebthings (Webthings para Ubports)

**Aplicación no oficial** desarrollada para Ubuntu Touch Movil, [(Ubports)](https://ubports.com/) excelente sistema operativo para dispositivos móviles, mantenido activamente por la comunidad y seguramente la única alternativa real a Android o iphone.

Esta aplicación ha sido desarrollada con la mejor intención para satisfacer una necesidad personal, **se encuentra en su primera fase de desarrollo y se esperan errores y cuelgues inesperados**, se ofrece 'tal cual' y no se acepta ninguna responsabilidad por sus posibles fallos. **Al usarla se aceptan ésta condiciones**.

La app permite manejar algunas características del [Gateway webthings](https://webthings.io/), que previamente debes tener instalado y funcionando en tu red local, puedes seguir [aquí las instrucciones necesarias](https://webthings.io/docs/gateway-getting-started-guide.html) para instalar la pasarela en una Raspberry pi.

# Características

* Conexión al gateway a traves de la red local y pasarela en internet (probado con la pasarela oficial, no testado con servidor privado)
* Visualización de los dispositivos presentes en el Gateway
* Visualización de las reglas establecidas
* Modificación de propiedades en los dispositivos (No están todos probados y se pueden esperar fallos)
* Reinicio del gateway

# Por hacer

* Activacion / Desactivacion de las reglas (Todavía no lo permite la API de webthings)
* Acceso a la configuración del Gateway
* Añadir / Eliminar dispositivos del Gateway
* Visualización de los registros en los dispositivos monitorizados
* Muchas más cosas....

# Ayuda al desarrollo de la aplicación
Puedes comunicar cualquier problema en Gitlab para ayudar a solucionarlo.
Si te parece interesante el proyecto puedes hacer una donación a la siguiente dirección de Bitcoin Cash (BCH)

bitcoincash:qzcrsu92drpaq7k5jh5wq7p75c9yp7xn0sd0wk9agr
![qr](./assets/Webthings.png)
